using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CircleWidget : MonoBehaviour
{
    private Image image;
    private Color activeColor = new Color(0.4815709f, 1f, 0.3443396f);
	private float activeDuration = 0.2f;
	private float deactiveDuration = 0.5f;
	private bool activated = false;
	private Sequence transformSequence;
	private Sequence colourSequence;

	private void Awake()
	{
		image = GetComponent<Image>();
	}

    public void Activate()
    {
		if(!activated)
		{
			transformSequence = DOTween.Sequence();
			colourSequence = DOTween.Sequence();
			transformSequence.Append(image.transform.DOPunchScale(new Vector3(0.4f, 0.4f, 0.4f), activeDuration, 1, 0));
			colourSequence.Append(image.DOColor(activeColor, activeDuration));
			transformSequence.Play();
			colourSequence.Play();
			activated = true;
		}
	}

	public void Deactivate()
	{
		if (activated)
		{
			KillSequence();
			transformSequence = DOTween.Sequence();
			colourSequence = DOTween.Sequence();

			transformSequence.Append(image.transform.DOPunchPosition(new Vector3(3f, 3f, 0), deactiveDuration));
			colourSequence.Append(image.DOColor(Color.red, deactiveDuration / 2)).Append(image.DOColor(Color.gray, deactiveDuration / 2));
			transformSequence.Play();
			colourSequence.Play();
			activated = false;
		}
	}

	private void KillSequence()
	{
		if (transformSequence != null)
		{
			transformSequence.Kill();
		}
		if(colourSequence != null)
		{
			colourSequence.Kill();
		}
		image.transform.localScale = new Vector3(1f, 1f, 1f);
	}

	public void Reset()
    {
		KillSequence();
		image.color = Color.gray;
		image.transform.localScale = new Vector3(1f, 1f, 1f);
		activated = false;
	}
}
