using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ProgressIndicator : MonoBehaviour
{
    [SerializeField] private RoundWidget roundWidget;
    [SerializeField] private CombinationWidget combinationWidget;
    
	public void ReceiveData(int combination, int round)
	{
		roundWidget.UpdateRound(round);
		combinationWidget.UpdateCombination(combination, round);
	}

	public void OnReset()
	{
		roundWidget.UpdateRound(0);
		combinationWidget.UpdateCombination(0, 0);
	}
}
