using UnityEngine;
using UnityEngine.UI;

public class CombinationWidget : MonoBehaviour
{
	[SerializeField] private CircleWidget[] circlesWidgets;

	public void UpdateCombination(int combination, int round)
	{
		for (int i = 0; i < circlesWidgets.Length; i++)
		{
			if (i <= round)
			{
				circlesWidgets[i].gameObject.SetActive(true);
				if(i < combination)
				{
					circlesWidgets[i].Activate();
				}
				else
				{
					circlesWidgets[i].Reset();
				}
			}
			else
			{
				circlesWidgets[i].gameObject.SetActive(false);
			}
		}
	}
}
