public static class LeftRightServiceLocator
{
	private static GameManager gameManager;
	private static AudioManager audioManager;

	public static GameManager GameManager => gameManager;
	public static AudioManager AudioManager => audioManager;

	public static void RegisterGameManager(GameManager g)
	{
		if (gameManager == null)
		{
			gameManager = g;
		}
	}

	public static void RegisterAudioManager(AudioManager a)
	{
		if(!audioManager)
		{
			audioManager = a;
		}
	}
}
