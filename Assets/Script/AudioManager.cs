using UnityEngine;

public class AudioManager : MonoBehaviour
{
	[SerializeField] private Sound[] sounds;
	private AudioSource audioSource;

	private void Awake()
	{
		audioSource = gameObject.AddComponent<AudioSource>();
		LeftRightServiceLocator.RegisterAudioManager(this);
	}

	public void Play(string name)
	{
        for (int i = 0, len = sounds.Length; i < len; i++)
        {
			if (sounds[i].Name == name)
			{
				audioSource.PlayOneShot(sounds[i].Clip, sounds[i].Volume);
				break;
			}
        }
    }
}
