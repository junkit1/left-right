using UnityEngine;

[System.Serializable]
public class Sound
{
    [SerializeField] private string audioName;
	public string Name => audioName;

	[SerializeField] [Range(0f, 1f)] private float volume = 1;
    public float Volume => volume;
    [SerializeField] private AudioClip clip;
    public AudioClip Clip => clip;
}
