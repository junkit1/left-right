using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuUI : MonoBehaviour
{
    [SerializeField] private Button startButton;
	[SerializeField] private Button infoButton;
	[SerializeField] private Button quitButton;
	[SerializeField] private TextMeshProUGUI timeRecord;
	[SerializeField] private GameObject infoPopUp;
	[SerializeField] private Button infoCloseButton;

	private void Awake()
	{
		startButton.onClick.AddListener(OnStartButton);
		infoButton.onClick.AddListener(OnInfoButton);
		quitButton.onClick.AddListener(OnQuitButton);
		infoCloseButton.onClick.AddListener(OnInfoClose);
		infoPopUp.SetActive(false);
	}

	void OnStartButton()
	{
		LeftRightServiceLocator.GameManager.TriggerStartGame();
		gameObject.SetActive(false);
	}

    public void Show(float t)
	{
		timeRecord.text = t == 0 ? "" : Utilis.GetTimeFormat(t);
		gameObject.SetActive(true);
	}

	private void OnInfoButton()
	{
		infoPopUp.SetActive(true);
	}

	private void OnInfoClose()
	{
		infoPopUp.SetActive(false);
	}

	private void OnQuitButton()
	{
		Application.Quit();
	}
}
