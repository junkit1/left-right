using DG.Tweening;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
    [SerializeField]
	private Button leftButton;
	[SerializeField]
	private Button rightButton;
	[SerializeField] private ProgressIndicator progressIndicator;
	[SerializeField]
	private TextMeshProUGUI timerText;

	private float timer = 0;
	private bool isStartRecordTime = false;

	// Start is called before the first frame update
	void Start()
    {
		LeftRightServiceLocator.GameManager.SubscribeStartGameEvent(OnStartGame);
		LeftRightServiceLocator.GameManager.SubscribeGameOverEvent(OnGameOver);
		LeftRightServiceLocator.GameManager.SubscribeResetEvent(OnReset);
		leftButton.onClick.AddListener(OnLeftButton);
        rightButton.onClick.AddListener(OnRightButton);
    }

	private void Update()
	{
		if (isStartRecordTime)
		{
			timer += Time.deltaTime;
			timerText.text = Utilis.GetTimeFormat(timer);
		}
	}

	private void OnStartGame(object o, EventArgs e)
	{
		EnableInput();
		InitiateTimer();
	}

	private void OnGameOver(object o, EventArgs e)
	{
		DisableInput();
		StopTimer();
	}

	private void OnReset(object o, EventArgs e)
	{
		timer = 0;
		progressIndicator.OnReset();
	}

	private void InitiateTimer()
	{
		isStartRecordTime = true;
	}

	private void StopTimer()
	{
		isStartRecordTime = false;
		LeftRightServiceLocator.GameManager.ReceiveTime(timer);
	}

	private void OnLeftButton()
    {
		LeftRightServiceLocator.GameManager.OnChoiceButton(Choice.Left);
	}

	private void OnRightButton()
	{
		LeftRightServiceLocator.GameManager.OnChoiceButton(Choice.Right);
	}

	public void triggerProgressUIUpdate(int combination, int round)
	{
		progressIndicator.ReceiveData(combination, round);
	}

	public void EnableInput()
	{
		leftButton.interactable = true;
		rightButton.interactable = true;
	}

	public void DisableInput()
	{
		leftButton.interactable = false;
		rightButton.interactable = false;
	}
}
