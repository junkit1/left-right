using System;
using System.Collections;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameUI gameUI;

    [SerializeField]
    private LeftRightManager leftRightManager;

    [SerializeField]
    private MenuUI menuUI;

    [SerializeField]
    private GameOverPopUp gameOverPopUp;

    private float currentTimeRecord;
    private float timer;
    private event EventHandler startGameEvent;
    private event EventHandler gameOverEvent;
	private event EventHandler resetEvent;
	private readonly string timeRecordKey = "timeRecord";

	private void Awake()
	{
        LeftRightServiceLocator.RegisterGameManager(this);
		currentTimeRecord = PlayerPrefs.GetFloat(timeRecordKey, 0);
	}

	private void Start()
	{
		menuUI.Show(currentTimeRecord);
	}

	public void TriggerStartGame()
    {
        StartGame();
	}

    private void StartGame()
    {
        if(startGameEvent != null)
        {
            startGameEvent.Invoke(this, null);
        }
    }

    public void SubscribeStartGameEvent(EventHandler eventHandler)
    {
        startGameEvent += eventHandler;
	}

	public void SubscribeGameOverEvent(EventHandler eventHandler)
	{
		gameOverEvent += eventHandler;
	}
	public void SubscribeResetEvent(EventHandler eventHandler)
	{
		resetEvent += eventHandler;
	}

	public void OnChoiceButton(Choice c)
	{
		leftRightManager.ReceiveChoice(c);
	}

    public void UpdateProgressUI(int combination, int round)
    {
        gameUI.triggerProgressUIUpdate(combination, round);
    }

    public void ReceiveTime(float t)
    {
        timer = t;
        if(timer < currentTimeRecord || currentTimeRecord == 0)
        {
            currentTimeRecord = t;
            PlayerPrefs.SetFloat(timeRecordKey, t);
        }
    }

    public void ShowMenu()
    {
        menuUI.Show(currentTimeRecord);
    }

	public void TriggerGameOver()
    {
        if(gameOverEvent != null) gameOverEvent.Invoke(this, null);
		gameOverPopUp.Show(timer);
	}

    public void TriggerReset()
    {
		if (resetEvent != null) resetEvent.Invoke(this, null);
        timer = 0;
	}
}
