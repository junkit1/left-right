using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPopUp : MonoBehaviour
{
	[SerializeField] private TextMeshProUGUI completionText;
	[SerializeField] private Button menuButton;
	private CanvasGroup canvasGroup;
	private Sequence fadeSequence;
	private float fadeDuration = 0.3f;
	private float delayDuration = 0.3f;
	private string gameOverAudioKey = "gameOver";

	private void Awake()
	{
		menuButton.onClick.AddListener(ReturnToMenu);
		canvasGroup = GetComponent<CanvasGroup>();
	}

	private void Start()
	{
		canvasGroup.alpha = 0;
		canvasGroup.blocksRaycasts = false;
	}


	public void Show(float t)
	{
		completionText.text = Utilis.GetTimeFormat(t);
		fadeSequence = DOTween.Sequence();
		fadeSequence.AppendInterval(delayDuration).AppendCallback(() => { LeftRightServiceLocator.AudioManager.Play(gameOverAudioKey); }).Append(canvasGroup.DOFade(1, fadeDuration)).AppendCallback(() => {
			canvasGroup.blocksRaycasts = true;
		});
		fadeSequence.Play();
	}

	private void ReturnToMenu()
	{
		LeftRightServiceLocator.GameManager.ShowMenu();
		LeftRightServiceLocator.GameManager.TriggerReset();
		if (fadeSequence != null)
		{
			fadeSequence.Kill();
		}
		canvasGroup.alpha = 0;
		canvasGroup.blocksRaycasts = false;
	}
}
