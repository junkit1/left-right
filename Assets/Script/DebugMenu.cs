using TMPro;
using UnityEngine;

public class DebugMenu : MonoBehaviour
{
    [SerializeField] private bool debugMode;
    [SerializeField] private TextMeshProUGUI answerText;
    private Canvas canvas;

	private void Awake()
	{
        canvas = GetComponent<Canvas>();
        canvas.enabled = debugMode;
	}

    public void ReceiveAnswer(Choice[][] answer)
    {
        answerText.text = "";
		for (int i = 0; i < answer.Length; i++)
        {
            for (int j = 0; j < answer[i].Length; j++)
            {
                answerText.text += GenerateChoiceText(answer[i][j]) + (j + 1 == answer[i].Length ? "\n" : " ");
            }
        }
    }

    private string GenerateChoiceText(Choice c)
    {
        return c == Choice.Left ? "Left" : "Right";
    }
}
