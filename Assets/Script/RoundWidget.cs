using UnityEngine;
using UnityEngine.UI;

public class RoundWidget : MonoBehaviour
{
	[SerializeField] private CircleWidget[] circlesWidgets;

    public void UpdateRound(int round)
    {
        for (int i = 0; i < circlesWidgets.Length; i++)
        {
            if(i < round)
            {
                circlesWidgets[i].Activate();
            }
            else
            {
                circlesWidgets[i].Deactivate();
            }
        }
    }
}
