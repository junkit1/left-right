using UnityEngine;

public static class Utilis
{
	public static bool FlipACoin()
	{
		return Random.Range(0, 2) == 0;
	}

	public static string GetTimeFormat(float t)
	{
		int minutes = Mathf.FloorToInt(t / 60);
		int seconds = (int)(t % 60);
		return string.Format("{0:00}:{1:00}", minutes, seconds);
	}
}
