using UnityEngine;

public enum Choice { Left, Right };

public class LeftRightManager : MonoBehaviour
{
    private Choice[][] choicesArray;
    [SerializeField] private DebugMenu debugMenu;
    [SerializeField] private int currentRound = 0;
	[SerializeField] private int combination = 0;
    private readonly int totalRound = 5;
	private readonly string correctAudioKey = "correct";
	private readonly string wrongAudioKey = "wrong";

    // Start is called before the first frame update
    void Start()
    {
		LeftRightServiceLocator.GameManager.UpdateProgressUI(combination, currentRound);
        LeftRightServiceLocator.GameManager.SubscribeResetEvent(OnReset);
		InitialiseChoices();
        debugMenu.ReceiveAnswer(choicesArray);
	}

	private void InitialiseChoices()
	{
		choicesArray = new Choice[totalRound][];
        for (int i = 0; i < totalRound; i++)
        {
            choicesArray[i] = new Choice[i+1];
            for (int j = 0; j < choicesArray[i].Length; j++)
            {
                choicesArray[i][j] = GetRandomChoice();
			}
        }
    }

    private Choice GetRandomChoice()
	{
		return Random.Range(0, 2) == 0 ? Choice.Left : Choice.Right;

	}

    public void ReceiveChoice(Choice choice)
    {
        ValidateCombination(choice);
	}

    private void ValidateCombination(Choice choice)
    {
        if (choice == choicesArray[currentRound][combination]) {
            if(combination >= currentRound)
            {
                if(currentRound < totalRound - 1)
                {
                    combination = 0;
					currentRound++;
                    LeftRightServiceLocator.GameManager.UpdateProgressUI(combination, currentRound);
                }
                else
                {
					currentRound++;
                    combination++;
					LeftRightServiceLocator.GameManager.UpdateProgressUI(combination, currentRound);
					LeftRightServiceLocator.GameManager.TriggerGameOver();
                }
            }
            else
            {
                combination++;
				LeftRightServiceLocator.GameManager.UpdateProgressUI(combination, currentRound);
			}
			LeftRightServiceLocator.AudioManager.Play(correctAudioKey);
		}
		else
        {
            combination = 0;
            currentRound = 0;
            LeftRightServiceLocator.AudioManager.Play(wrongAudioKey);
			LeftRightServiceLocator.GameManager.UpdateProgressUI(combination, currentRound);
		}
	}

	private void OnReset(object o, System.EventArgs args)
	{
		currentRound = 0;
        combination = 0;
	}
}

